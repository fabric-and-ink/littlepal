#[test]
fn parse_examples() {
    use super::parse;
    use glob::glob;
    use std::fs;

    let mut count = 0;

    for entry in glob("../examples/**/*.vp").expect("Failed to read glob pattern") {
        let path = entry.unwrap();
        println!("Parsing {}", path.display());
        let content = fs::read_to_string(path).unwrap();
        parse(&content).unwrap();
        count += 1;
    }

    assert!(count > 0);
}
