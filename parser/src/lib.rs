#[macro_use]
extern crate lalrpop_util;

#[macro_use]
extern crate anyhow;

use anyhow::Result;
pub use ast::Model;

lalrpop_mod!(
    #[allow(clippy::all)]
    #[allow(dead_code)]
    grammar
);

pub mod ast;

pub fn parse(input: &str) -> Result<Model<String>> {
    match grammar::ModelParser::new().parse(input) {
        Ok(a) => Ok(a),
        Err(e) => Err(anyhow!("{}", e)),
    }
}

#[cfg(test)]
mod test;
