#[derive(Debug)]
pub struct Model<T> {
    pub attacker: Attacker,
    pub blocks: Vec<Block<T>>,
    pub queries: Vec<Query<T>>,
}

impl<T> Model<T> {
    pub fn new(attacker: Attacker, blocks: Vec<Block<T>>, queries: Vec<Query<T>>) -> Self {
        Self {
            attacker,
            blocks,
            queries,
        }
    }
}

#[derive(Debug)]
pub enum Block<T> {
    Message(Message<T>),
    Phase(Phase),
    Principal(Principal<T>),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Attacker {
    Active,
    Passive,
}

#[derive(Debug)]
pub struct Principal<T> {
    pub name: T,
    pub expressions: Vec<Expression<T>>,
}

impl<T> Principal<T> {
    pub fn new(name: T, expressions: Vec<Expression<T>>) -> Self {
        Self { name, expressions }
    }
}

#[derive(Debug)]
pub enum Expression<T> {
    Assignment(Vec<LValue<T>>, RValue<T>),
    Generates(Vec<T>),
    Knows(Vec<T>, Qualifier),
    Leaks(Vec<T>),
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Qualifier {
    Public,
    Private,
    Password,
}

#[derive(Debug)]
pub enum RValue<T> {
    Nil,
    Generator,
    Constant(T),
    Function(Function<T>),
}

#[derive(Debug)]
pub enum LValue<T> {
    Dump,
    Constant(T),
}

#[derive(Debug)]
pub struct Function<T> {
    pub name: T,
    pub parameters: Vec<RValue<T>>,
    pub check: bool,
}

impl<T> Function<T> {
    pub fn new(name: T, parameters: Vec<RValue<T>>, check: bool) -> Self {
        Self {
            name,
            parameters,
            check,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Message<T> {
    pub sender: T,
    pub receiver: T,
    pub packages: Vec<Package<T>>,
}

impl<T> Message<T> {
    pub fn new(sender: T, receiver: T, packages: Vec<Package<T>>) -> Self {
        Self {
            sender,
            receiver,
            packages,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Package<T> {
    Unguarded(T),
    Guarded(T),
}

#[derive(Debug)]
pub struct Phase {
    pub name: String,
}

impl Phase {
    pub fn new(name: String) -> Self {
        Self { name }
    }
}

#[derive(Debug)]
pub struct Query<T> {
    pub name: T,
    pub inverted: bool,
    pub target: QueryTarget<T>,
    pub if_true_msg: Option<Message<T>>,
}

impl<T> Query<T> {
    pub fn new(
        name: T,
        inverted: bool,
        target: QueryTarget<T>,
        if_true_msg: Option<Message<T>>,
    ) -> Self {
        Self {
            name,
            inverted,
            target,
            if_true_msg,
        }
    }
}

#[derive(Debug)]
pub enum QueryTarget<T> {
    Constants(Vec<T>),
    Message(Message<T>),
}
