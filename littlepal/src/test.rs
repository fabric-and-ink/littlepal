use test_log::test;

#[test]
fn check_tests_simple_enc() {
    use super::{analyze, parse};
    use glob::glob;

    let mut count = 0;
    let mut is_ok = true;
    let mut test_count = 0;
    let mut failure_count = 0;

    for entry in glob("../test_input/simple_enc/*.vp").expect("Failed to read glob pattern") {
        let path = entry.unwrap();
        println!("Check file {}", path.display());

        let model = parse(&path).unwrap();
        let model = analyze(model);
        let is_this_test_ok = model.check_queries();
        is_ok &= is_this_test_ok;
        test_count += 1;
        if !is_this_test_ok {
            println!("Check failed");
            failure_count += 1;
        }

        count += 1;
    }

    println!("Failure rate {} of {} tests", failure_count, test_count);
    assert!(count > 0);
    assert!(is_ok);
}

#[test]
fn check_tests_trivial() {
    use super::{analyze, parse};
    use glob::glob;

    let mut count = 0;
    let mut is_ok = true;
    let mut test_count = 0;
    let mut failure_count = 0;

    for entry in glob("../test_input/trivial/*.vp").expect("Failed to read glob pattern") {
        let path = entry.unwrap();
        println!("Check file {}", path.display());

        let model = parse(&path).unwrap();
        let model = analyze(model);
        let is_this_test_ok = model.check_queries();
        is_ok &= is_this_test_ok;
        test_count += 1;
        if !is_this_test_ok {
            println!("Check failed");
            failure_count += 1;
        }

        count += 1;
    }

    println!("Failure rate {} of {} tests", failure_count, test_count);
    assert!(count > 0);
    assert!(is_ok);
}

#[test]
fn check_tests_simple_aead_enc() {
    use super::{analyze, parse};
    use glob::glob;

    let mut count = 0;
    let mut is_ok = true;
    let mut test_count = 0;
    let mut failure_count = 0;

    for entry in glob("../test_input/simple_aead_enc/*.vp").expect("Failed to read glob pattern") {
        let path = entry.unwrap();
        println!("Check file {}", path.display());

        let model = parse(&path).unwrap();
        let model = analyze(model);
        let is_this_test_ok = model.check_queries();
        is_ok &= is_this_test_ok;
        test_count += 1;
        if !is_this_test_ok {
            println!("Check failed");
            failure_count += 1;
        }

        count += 1;
    }

    println!("Failure rate {} of {} tests", failure_count, test_count);
    assert!(count > 0);
    assert!(is_ok);
}
