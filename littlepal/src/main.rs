use anyhow::Result;
use clap::{Arg, Command};
use fs::File;
use simplelog::*;
use std::{fs, path::Path};

#[cfg(test)]
mod test;

fn parse(filename: &Path) -> Result<parser::Model<String>> {
    let content = fs::read_to_string(filename)?;
    parser::parse(&content)
}

fn analyze(model: parser::Model<String>) -> analyzer::RewritingAnalysis {
    let model = analyzer::IndexedIdentsModel::new(model);
    let model = analyzer::FlattenedModel::new(model);
    let model = analyzer::FlowModel::new(model);
    analyzer::RewritingAnalysis::new(model)
}

fn main() -> Result<()> {
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Info,
            ConfigBuilder::default()
                .add_filter_allow_str("analyzer")
                .build(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Trace,
            Config::default(),
            File::create("run.log").unwrap(),
        ),
    ])
    .unwrap();
    let matches = Command::new("littlepal")
        .arg(Arg::new("file").short('f').required(true).takes_value(true))
        .get_matches();
    let filename = Path::new(matches.value_of("file").unwrap());
    let model = parse(filename)?;
    let model = analyze(model);

    fs::write("data_flow.dot", model.flow.get_dot())?;
    model.attacker_egraph_to_dot("egraph.dot")?;

    Ok(())
}
