use std::fmt;

use fmt::Display;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum QueryKind {
    Authentication,
    Confidentiality,
    Unlinkability,
    Freshness,
    Equivalence,
}

impl QueryKind {
    pub fn new(name: String) -> Self {
        match name.as_str() {
            "authentication" => QueryKind::Authentication,
            "confidentiality" => QueryKind::Confidentiality,
            "unlinkability" => QueryKind::Unlinkability,
            "freshness" => QueryKind::Freshness,
            "equivalence" => QueryKind::Equivalence,
            _ => unimplemented!("Query {} is not implemented", name),
        }
    }
}

impl Display for QueryKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
