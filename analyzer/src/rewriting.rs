use crate::primitives::PrimitiveKind;
use crate::{flow::*, queries::QueryKind};
use egg::*;
use log::*;
use parser::ast::Attacker;
use petgraph::visit::Topo;
use petgraph::{prelude::*, visit::IntoNodeReferences};
use std::collections::HashSet;
use std::{collections::HashMap, convert::TryInto, path::Path};

define_language! {
    pub enum Code {
        Sym(Symbol),

        "UnGuard" = UnGuard(Id),
        "Auth" = Auth(Id),
        "Pub" = Pub(Id),
        "Subst" = Subst(Id),

        "Enc" = Enc([Id; 2]),
        "Dec" = Dec([Id; 2]),

        "AeadEnc" = AeadEnc([Id; 3]),
        "AeadDec" = AeadDec([Id; 3]),

        "PkeEnc" = PkeEnc([Id; 2]),
        "PkeDec" = PkeDec([Id; 2]),

        "Exp" = Exp([Id; 2]),

        "Hash1" = Hash1([Id; 1]),
        "Hash2" = Hash2([Id; 2]),
        "Hash3" = Hash3([Id; 3]),
        "Hash4" = Hash4([Id; 4]),
        "Hash5" = Hash5([Id; 5]),

        "PwHash1" = PwHash1([Id; 1]),
        "PwHash2" = PwHash2([Id; 2]),
        "PwHash3" = PwHash3([Id; 3]),
        "PwHash4" = PwHash4([Id; 4]),
        "PwHash5" = PwHash5([Id; 5]),

        "Hkdf1" = Hkdf1([Id; 3]),
        "Hkdf2" = Hkdf2([Id; 3]),
        "Hkdf3" = Hkdf3([Id; 3]),
        "Hkdf4" = Hkdf4([Id; 3]),
        "Hkdf5" = Hkdf5([Id; 3]),

        "Concat1" = Concat1([Id; 1]),
        "Concat2" = Concat2([Id; 2]),
        "Concat3" = Concat3([Id; 3]),
        "Concat4" = Concat4([Id; 4]),
        "Concat5" = Concat5([Id; 5]),

        "Split1" = Split1(Id),
        "Split2" = Split2(Id),
        "Split3" = Split3(Id),
        "Split4" = Split4(Id),
        "Split5" = Split5(Id),

        "RingSign" = RingSign([Id; 4]),
        "RingSignVerif" = RingSignVerif([Id; 5]),

        "Sign" = Sign([Id; 2]),
        "SignVerif" = SignVerif([Id; 3]),

        "Mac" = Mac([Id; 2]),

        "Blind" = Blind([Id; 2]),
        "UnBlind" = UnBlind([Id; 3]),

        "ShamirSplit1" = ShamirSplit1(Id),
        "ShamirSplit2" = ShamirSplit2(Id),
        "ShamirSplit3" = ShamirSplit3(Id),
        "ShamirJoin" = ShamirJoin([Id; 2]),

        "Assert" = Assert([Id; 2]),
    }
}

pub struct RewritingAnalysis {
    pub flow: FlowModel,
    pub egraph: EGraph<Code, ()>,
    pub flow_to_egraph: HashMap<NodeIndex, Id>,
    pub const_id_attack_map: HashMap<usize, Id>,
    pub unguarded_origin: HashMap<Id, Id>,
}

impl RewritingAnalysis {
    pub fn new(flow: FlowModel) -> Self {
        let mut result = Self {
            flow,
            egraph: EGraph::new(()),
            flow_to_egraph: HashMap::default(),
            const_id_attack_map: HashMap::default(),
            unguarded_origin: HashMap::default(),
        };
        result.insert_constants();
        result.insert_primitives();
        // TODO sending one message to two receivers doesn't work,
        // since the respective transmissions would be treated as equivalent.
        result.insert_assignments();
        result.run_analysis()
    }

    pub fn attacker_egraph_to_dot(&self, filename: impl AsRef<Path>) -> std::io::Result<()> {
        self.egraph
            .dot()
            //.with_config_line("rankdir=LR")
            //.with_config_line("ranksep=4")
            //.with_anchors(false)
            .to_dot(filename)
    }

    pub fn dump(&self) {
        println!("Attackers egraph");
        println!("{:?}", self.egraph.dump());
    }

    fn insert_constants(&mut self) {
        for (node_id, node_weight) in self.flow.graph.node_references() {
            match *node_weight {
                FlowNode::Generator => {
                    let g = Code::Sym("#g".into());
                    let id = self.egraph.add(g);
                    self.egraph.add(Code::Auth(id));
                    self.flow_to_egraph.insert(node_id, id);
                }

                FlowNode::Nil => {
                    let nil = Code::Sym("#nil".into());
                    let id = self.egraph.add(nil);
                    self.egraph.add(Code::Auth(id));
                    self.flow_to_egraph.insert(node_id, id);
                }

                FlowNode::Generate(_, constant_id) => {
                    let constant_name = &self.flow.lookup.idents_const[constant_id];
                    let symbol_name = constant_name.to_string();
                    let symbol = Code::Sym(symbol_name.into());

                    let id = self.egraph.add(symbol);
                    self.egraph.add(Code::Auth(id));
                    self.const_id_attack_map.insert(constant_id, id);
                    self.flow_to_egraph.insert(node_id, id);
                }

                FlowNode::Binding(_, constant_id) => {
                    let constant_name = &self.flow.lookup.idents_const[constant_id];
                    let symbol_name = constant_name.to_string();
                    let symbol = Code::Sym(symbol_name.into());

                    let egraph_id = self.egraph.add(symbol);
                    self.egraph.add(Code::Auth(egraph_id));
                    self.const_id_attack_map.insert(constant_id, egraph_id);
                    self.flow_to_egraph.insert(node_id, egraph_id);
                }

                FlowNode::Knows(_, constant_id, _) => {
                    let symbol = {
                        let constant_name = &self.flow.lookup.idents_const[constant_id];
                        let symbol_name = constant_name.to_string();
                        Code::Sym(symbol_name.into())
                    };

                    let attack_id = self.egraph.add(symbol);
                    self.egraph.add(Code::Auth(attack_id));
                    self.const_id_attack_map.insert(constant_id, attack_id);
                    self.flow_to_egraph.insert(node_id, attack_id);
                }

                FlowNode::Free(constant_id) => {
                    let constant_name = &self.flow.lookup.idents_const[constant_id];
                    let symbol_name = constant_name.to_string();
                    let symbol = Code::Sym(symbol_name.into());
                    let id = self.egraph.add(symbol);
                    self.flow_to_egraph.insert(node_id, id);
                    self.const_id_attack_map.insert(constant_id, id);
                }

                FlowNode::Leaks(_, _) => {
                    unreachable!()
                }
                _ => (),
            }
        }
    }

    fn get_primitives(&self) -> Vec<(NodeIndex, PrimitiveKind, Vec<NodeIndex>, usize)> {
        let mut result = Vec::new();
        let g = &self.flow.graph;
        let mut topo = Topo::new(g);
        while let Some(node_id) = topo.next(g) {
            let node_weight = g.node_weight(node_id).unwrap();
            if let FlowNode::FunctionSingleReturn(_, n, out) = *node_weight {
                let mut args: Vec<_> = self
                    .flow
                    .graph
                    .edges_directed(node_id, Direction::Incoming)
                    .map(|e| {
                        let (s, _) = self.flow.graph.edge_endpoints(e.id()).unwrap();
                        let arg_idx = self
                            .flow
                            .graph
                            .edge_weight(e.id())
                            .unwrap()
                            .get_fn_arg_idx();
                        (arg_idx, s)
                    })
                    .collect();

                args.sort_by(|a, b| a.0.cmp(&b.0));
                let args: Vec<_> = args.into_iter().map(|(_, id)| id).collect();
                let fun = &self.flow.fn_calls[n];
                let p = self.flow.lookup.idents_fns[fun.name];
                result.push((node_id, p, args, out));
            }
        }
        result
    }

    fn insert_primitives(&mut self) {
        for (node_id, p, args, out) in self.get_primitives() {
            let args: Vec<_> = args.into_iter().map(|a| self.flow_to_egraph[&a]).collect();

            let attacker_id = insert_primitive(&mut self.egraph, p, &args, out);
            self.flow_to_egraph.insert(node_id, attacker_id);
        }
    }

    fn insert_assignments(&mut self) {
        for edge_id in self.flow.graph.edge_indices() {
            let (start_flow, end_flow) = self.flow.graph.edge_endpoints(edge_id).unwrap();
            if self.flow_to_egraph.contains_key(&start_flow)
                && self.flow_to_egraph.contains_key(&end_flow)
            {
                let start_egr = self.flow_to_egraph[&start_flow];
                let end_egr = self.flow_to_egraph[&end_flow];

                match self.flow.graph.edge_weight(edge_id).unwrap() {
                    FlowEdge::TransmissionUnguarded => {
                        self.unguarded_origin.insert(end_egr, start_egr);
                        self.egraph.add(Code::Pub(start_egr));

                        // This should essentially distinguish an
                        // active from a passive attacker. TODO actually test this.
                        let mut start_egr = start_egr;
                        if self.flow.attacker == Attacker::Active {
                            start_egr = self.egraph.add(Code::Subst(start_egr));
                        }

                        self.egraph.union(start_egr, end_egr);
                    }

                    FlowEdge::TransmissionGuarded => {
                        self.egraph.add(Code::Pub(start_egr));
                        self.egraph.add(Code::Auth(start_egr));
                        self.egraph.union(start_egr, end_egr);
                    }

                    FlowEdge::InternalAssignment | FlowEdge::FunctionOutput(_) => {
                        self.egraph.add(Code::Auth(start_egr));
                        self.egraph.union(start_egr, end_egr);
                    }
                    _ => {}
                }
            }
        }
    }

    fn get_primitives_and_arg_count(&self) -> HashSet<(PrimitiveKind, Option<usize>)> {
        let mut result = HashSet::new();
        for f in &self.flow.fn_calls {
            let prim = &self.flow.lookup.idents_fns[f.name];
            if matches!(prim, PrimitiveKind::Concat | PrimitiveKind::Hash) {
                result.insert((*prim, Some(f.parameters.len())));
            } else {
                result.insert((*prim, None));
            }
        }
        result
    }

    pub fn check_queries(&self) -> bool {
        let mut is_ok = true;
        for q in &self.flow.queries {
            let query = self.flow.lookup.idents_queries[q.name];

            if q.inverted {
                warn!("Checking negated query {}", query);
            } else {
                info!("Checking query {}", query);
            }

            match query {
                QueryKind::Confidentiality => {
                    for const_id in q.get_consts_id() {
                        info!("Constant name {}", self.flow.lookup.idents_const[*const_id]);
                        let target_id = self.const_id_attack_map[const_id];
                        info!("Egraph id {}", target_id);
                        let code_pub = Code::Pub(target_id);
                        let is_pub = self.egraph.lookup(code_pub).is_some();
                        if q.inverted {
                            if !is_pub {
                                error!("Check failed");
                                is_ok = false;
                            } else {
                                info!("Check succeeded");
                            }
                        } else if is_pub {
                            error!("Check failed");
                            is_ok = false;
                        } else {
                            info!("Check succeeded");
                        }
                    }
                }
                QueryKind::Authentication => {
                    let const_id = q.get_tr_const_id();
                    info!("Constant name {}", self.flow.lookup.idents_const[const_id]);
                    let mut target_id = self.const_id_attack_map[&const_id];
                    target_id = self.egraph.find(target_id);
                    info!("Egraph id {}", target_id);
                    let code_fix = Code::Auth(target_id);
                    let is_auth = self.egraph.lookup(code_fix).is_some();
                    if q.inverted {
                        if is_auth {
                            error!("Check failed");
                            is_ok = false;
                        } else {
                            info!("Check succeeded");
                        }
                    } else if !is_auth {
                        error!("Check failed");
                        is_ok = false;
                    } else {
                        info!("Check succeeded");
                    }
                }
                QueryKind::Equivalence => {
                    let consts = q.get_consts_id();
                    if consts.len() != 2 {
                        panic!("Equivalence query expects two consts");
                    }
                    let c0 = consts[0];
                    let c1 = consts[1];
                    let mut id0 = self.const_id_attack_map[&c0];
                    info!("Constant name {}", self.flow.lookup.idents_const[c0]);
                    info!("Egraph id {}", id0);
                    let mut id1 = self.const_id_attack_map[&c1];
                    info!("Constant name {}", self.flow.lookup.idents_const[c1]);
                    info!("Egraph id {}", id1);
                    id0 = self.egraph.find(id0);
                    id1 = self.egraph.find(id1);
                    if q.inverted {
                        if id0 == id1 {
                            error!("Check failed");
                            is_ok = false;
                        } else {
                            info!("Check succeeded");
                        }
                    } else if id0 != id1 {
                        error!("Check failed");
                        is_ok = false;
                    } else {
                        info!("Check succeeded");
                    }
                }
                _ => {
                    warn!("Query {} not implemented yet", query);
                }
            }
        }
        is_ok
    }

    fn select_rules(&self) -> Vec<Rewrite<Code, ()>> {
        let prims = self.get_primitives_and_arg_count();
        let mut result = vec![];

        if prims.contains(&(PrimitiveKind::Enc, None)) {
            result.push(rewrite!("decrypt";
                "(Dec ?pw (Enc ?pw ?msg))" => "?msg"));

            result.push(multi_rewrite!("decrypt_public";
                "?ct = (Enc ?pw ?msg), \
                 ?ppw = (Pub ?pw), \
                 ?pct = (Pub ?ct)"
                => "?pmsg = (Pub ?msg)"));

            result.push(multi_rewrite!("decrypt_subst";
                "?ct = (Enc (Subst ?pw) ?msg), \
                 ?pct = (Pub ?ct)"
                => "?pmsg = (Pub ?msg)"));

            result.push(multi_rewrite!("decrypt_auth";
                "?pt = (Dec ?pw (Subst ?ct)), \
                 ?ct = (Enc ?pw ?msg), \
                 ?apw = (Auth ?pw)"
                => "?pt = ?msg, ?asct = (Auth (Subst ?ct))"));
        }

        if prims.contains(&(PrimitiveKind::AeadEnc, None)) {
            result.push(rewrite!("decrypt_aead";
                "(AeadDec ?pw (AeadEnc ?pw ?msg ?aad) ?aad)" => "?msg"));

            result.push(multi_rewrite!("decrypt_aead_public";
                "?ct = (AeadEnc ?pw ?msg ?aad), \
                 ?ppw = (Pub ?pw), \
                 ?pct = (Pub ?ct)"
                => "?pmsg = (Pub ?msg)"));

            result.push(multi_rewrite!("decrypt_aead_subst";
                "?ct = (AeadEnc (Subst ?pw) ?msg ?aad), \
                 ?pct = (Pub ?ct)"
                => "?pmsg = (Pub ?msg)"));

            result.push(multi_rewrite!("decrypt_aead_auth";
                "?pt = (AeadDec ?pw ?ct ?aad), \
                 ?apw = (Auth ?pw)"
                => "?asct = (Auth ?ct), \
                    ?aaad = (Auth ?aad)"));
        }

        if prims.contains(&(PrimitiveKind::PkeEnc, None)) {
            result.push(rewrite!("decrypt_pke_private";
                "(PkeDec ?key (PkeEnc (Exp ?base ?key) ?msg))" => "?msg"));

            result.push(rewrite!("decrypt_pke_subst";
                "(PkeEnc (Pub #Nil) ?msg)" => "?msg"));
        }

        if prims.contains(&(PrimitiveKind::Exp, None)) {
            result.push(rewrite!("exp_com";
                "(Exp (Exp ?g ?a) ?b)" => "(Exp (Exp ?g ?b) ?a)"));

            result.push(multi_rewrite!("exp_itm";
                "?ga = (Exp ?g ?a), \
                 ?gsab = (Exp (Subst ?ga) ?b), \
                 ?pgb = (Pub (Exp ?g ?b))" => "?pgsab = (Pub ?gsab)"));
        }

        if prims.contains(&(PrimitiveKind::Sign, None)) {
            result.push(rewrite!("sign_verif";
                "(SignVerif (Exp ?base ?key) ?msg (Sign ?key ?msg))" => "?msg"));
        }

        if prims.contains(&(PrimitiveKind::RingSign, None)) {
            result.push(rewrite!("ring_sign_verif";
            "(RingSignVerif \
                (Exp ?base ?key1) \
                (Exp ?base ?key2) \
                (Exp ?base ?key3) \
                ?msg \
                (RingSign \
                    ?key1 \
                    (Exp ?base ?key2) \
                    (Exp ?base ?key3) \
                    ?msg))"
            => "?msg"));
        }

        if prims.contains(&(PrimitiveKind::Blind, None)) {
            result.push(
                rewrite!("unblind"; "(UnBlind ?k ?m (Sign ?a (Blind ?k ?m)))" => "(Sign ?a ?m)"),
            );
        }

        if prims.contains(&(PrimitiveKind::ShamirSplit, None)) {
            result.push(rewrite!("shamir_comm"; "(ShamirJoin ?a ?b)" => "(ShamirJoin ?b ?a)"));
            result.push(
                rewrite!("shamir_1"; "(ShamirJoin (ShamirSplit1 ?x) (ShamirSplit2 ?x))" => "?x"),
            );
            result.push(
                rewrite!("shamir_2"; "(ShamirJoin (ShamirSplit1 ?x) (ShamirSplit3 ?x))" => "?x"),
            );
            result.push(
                rewrite!("shamir_3"; "(ShamirJoin (ShamirSplit2 ?x) (ShamirSplit3 ?x))" => "?x"),
            );
        }

        if prims.contains(&(PrimitiveKind::Concat, Some(1))) {
            result.push(rewrite!("split_1"; "(Split1 (Concat1 ?x))" => "?x"));
            result.push(rewrite!("concat_pub_1"; "(Pub (Concat1 ?x))"
                => "(Pub (Concat1 (Pub ?x)))"));
        }

        if prims.contains(&(PrimitiveKind::Concat, Some(2))) {
            result.push(rewrite!("split_2_1"; "(Split1 (Concat2 ?x ?y))" => "?x"));
            result.push(rewrite!("split_2_2"; "(Split2 (Concat2 ?x ?y))" => "?y"));
        }

        if prims.contains(&(PrimitiveKind::Concat, Some(3))) {
            result.push(rewrite!("split_3_1"; "(Split1 (Concat3 ?x ?y ?z))" => "?x"));
            result.push(rewrite!("split_3_2"; "(Split2 (Concat3 ?x ?y ?z))" => "?y"));
            result.push(rewrite!("split_3_3"; "(Split3 (Concat3 ?x ?y ?z))" => "?z"));
        }

        if prims.contains(&(PrimitiveKind::Concat, Some(4))) {
            result.push(rewrite!("split_4_1"; "(Split1 (Concat4 ?x ?y ?z ?a))" => "?x"));
            result.push(rewrite!("split_4_2"; "(Split2 (Concat4 ?x ?y ?z ?a))" => "?y"));
            result.push(rewrite!("split_4_3"; "(Split3 (Concat4 ?x ?y ?z ?a))" => "?z"));
            result.push(rewrite!("split_4_4"; "(Split4 (Concat4 ?x ?y ?z ?a))" => "?a"));
        }

        if prims.contains(&(PrimitiveKind::Concat, Some(5))) {
            result.push(rewrite!("split_5_1"; "(Split1 (Concat5 ?x ?y ?z ?a ?b))" => "?x"));
            result.push(rewrite!("split_5_2"; "(Split2 (Concat5 ?x ?y ?z ?a ?b))" => "?y"));
            result.push(rewrite!("split_5_3"; "(Split3 (Concat5 ?x ?y ?z ?a ?b))" => "?z"));
            result.push(rewrite!("split_5_4"; "(Split4 (Concat5 ?x ?y ?z ?a ?b))" => "?a"));
            result.push(rewrite!("split_5_5"; "(Split5 (Concat5 ?x ?y ?z ?a ?b))" => "?b"));
        }
        result
    }

    pub fn run_analysis(mut self) -> Self {
        let rules = self.select_rules();

        let run = Runner::default().with_egraph(self.egraph).run(&rules);

        println!("{:?}", run.egraph.dump());
        self.egraph = run.egraph;
        self
    }
}

fn insert_primitive(
    egraph: &mut EGraph<Code, ()>,
    p: PrimitiveKind,
    args: &[Id],
    out: usize,
) -> Id {
    match p {
        PrimitiveKind::Exp => egraph.add(Code::Exp(args.try_into().unwrap())),
        PrimitiveKind::AeadEnc => egraph.add(Code::AeadEnc(args.try_into().unwrap())),
        PrimitiveKind::AeadDec => egraph.add(Code::AeadDec(args.try_into().unwrap())),
        PrimitiveKind::Hash => match args.len() {
            1 => egraph.add(Code::Hash1(args.try_into().unwrap())),
            2 => egraph.add(Code::Hash2(args.try_into().unwrap())),
            3 => egraph.add(Code::Hash3(args.try_into().unwrap())),
            4 => egraph.add(Code::Hash4(args.try_into().unwrap())),
            5 => egraph.add(Code::Hash5(args.try_into().unwrap())),
            _ => unreachable!(),
        },
        PrimitiveKind::PwHash => match args.len() {
            1 => egraph.add(Code::PwHash1(args.try_into().unwrap())),
            2 => egraph.add(Code::PwHash2(args.try_into().unwrap())),
            3 => egraph.add(Code::PwHash3(args.try_into().unwrap())),
            4 => egraph.add(Code::PwHash4(args.try_into().unwrap())),
            5 => egraph.add(Code::PwHash5(args.try_into().unwrap())),
            _ => unreachable!(),
        },
        PrimitiveKind::Concat => match args.len() {
            1 => egraph.add(Code::Concat1(args.try_into().unwrap())),
            2 => egraph.add(Code::Concat2(args.try_into().unwrap())),
            3 => egraph.add(Code::Concat3(args.try_into().unwrap())),
            4 => egraph.add(Code::Concat4(args.try_into().unwrap())),
            5 => egraph.add(Code::Concat5(args.try_into().unwrap())),
            _ => unreachable!(),
        },
        PrimitiveKind::Hkdf => match out {
            0 => egraph.add(Code::Hkdf1(args.try_into().unwrap())),
            1 => egraph.add(Code::Hkdf2(args.try_into().unwrap())),
            2 => egraph.add(Code::Hkdf3(args.try_into().unwrap())),
            3 => egraph.add(Code::Hkdf4(args.try_into().unwrap())),
            4 => egraph.add(Code::Hkdf5(args.try_into().unwrap())),
            _ => unreachable!(),
        },
        PrimitiveKind::Sign => egraph.add(Code::Sign(args.try_into().unwrap())),
        PrimitiveKind::SignVerif => egraph.add(Code::SignVerif(args.try_into().unwrap())),
        PrimitiveKind::RingSign => egraph.add(Code::RingSign(args.try_into().unwrap())),
        PrimitiveKind::RingSignVerif => egraph.add(Code::RingSignVerif(args.try_into().unwrap())),
        PrimitiveKind::Enc => egraph.add(Code::Enc(args.try_into().unwrap())),
        PrimitiveKind::Dec => egraph.add(Code::Dec(args.try_into().unwrap())),
        PrimitiveKind::PkeEnc => egraph.add(Code::PkeEnc(args.try_into().unwrap())),
        PrimitiveKind::PkeDec => egraph.add(Code::PkeDec(args.try_into().unwrap())),
        PrimitiveKind::Mac => egraph.add(Code::Mac(args.try_into().unwrap())),
        PrimitiveKind::Blind => egraph.add(Code::Blind(args.try_into().unwrap())),
        PrimitiveKind::UnBlind => egraph.add(Code::UnBlind(args.try_into().unwrap())),
        PrimitiveKind::Split => match out {
            0 => egraph.add(Code::Split1(args[0])),
            1 => egraph.add(Code::Split2(args[0])),
            2 => egraph.add(Code::Split3(args[0])),
            3 => egraph.add(Code::Split4(args[0])),
            4 => egraph.add(Code::Split5(args[0])),
            _ => unimplemented!(),
        },
        PrimitiveKind::ShamirSplit => match out {
            0 => egraph.add(Code::ShamirSplit1(args[0])),
            1 => egraph.add(Code::ShamirSplit2(args[0])),
            2 => egraph.add(Code::ShamirSplit3(args[0])),
            _ => unimplemented!(),
        },
        PrimitiveKind::ShamirJoin => egraph.add(Code::ShamirJoin(args.try_into().unwrap())),
        PrimitiveKind::Assert => egraph.add(Code::Assert(args.try_into().unwrap())),
    }
}
