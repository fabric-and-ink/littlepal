use crate::primitives::PrimitiveKind;
use crate::queries::QueryKind;
use fxhash::FxBuildHasher;
use indexmap::IndexSet;

type StringLookup = IndexSet<String, FxBuildHasher>;
type PrimLookup = IndexSet<PrimitiveKind, FxBuildHasher>;
type QueryLookup = IndexSet<QueryKind, FxBuildHasher>;

#[derive(Debug)]
pub struct IdentLookup {
    pub idents_const: StringLookup,
    pub idents_fns: PrimLookup,
    pub idents_queries: QueryLookup,
    pub idents_pr: StringLookup,
}

impl IdentLookup {
    pub fn new() -> Self {
        Self {
            idents_const: StringLookup::default(),
            idents_fns: PrimLookup::default(),
            idents_queries: QueryLookup::default(),
            idents_pr: StringLookup::default(),
        }
    }

    pub fn insert(&mut self, kind: IdentKind, name: String) -> usize {
        match kind {
            IdentKind::Constant => insert(name, &mut self.idents_const),
            IdentKind::Function => insert_primitive(name, &mut self.idents_fns),
            IdentKind::Query => insert_query(name, &mut self.idents_queries),
            IdentKind::Principal => insert(name, &mut self.idents_pr),
        }
    }

    pub fn get_string(&self, kind: IdentKind, idx: usize) -> String {
        match kind {
            IdentKind::Constant => self.idents_const[idx].clone(),
            IdentKind::Function => self.idents_fns[idx].to_string(),
            IdentKind::Query => self.idents_queries[idx].to_string(),
            IdentKind::Principal => self.idents_pr[idx].clone(),
        }
    }

    pub fn insert_many(&mut self, kind: IdentKind, names: Vec<String>) -> Vec<usize> {
        names.into_iter().map(|n| self.insert(kind, n)).collect()
    }
}

impl Default for IdentLookup {
    fn default() -> Self {
        Self::new()
    }
}

fn insert(name: String, set: &mut StringLookup) -> usize {
    let (idx, _) = set.insert_full(name);
    idx
}

fn insert_primitive(primitive: String, set: &mut PrimLookup) -> usize {
    let (idx, _) = set.insert_full(PrimitiveKind::new(primitive));
    idx
}

fn insert_query(query: String, set: &mut QueryLookup) -> usize {
    let (idx, _) = set.insert_full(QueryKind::new(query));
    idx
}

#[derive(Clone, Copy)]
pub enum IdentKind {
    Constant,
    Function,
    Query,
    Principal,
}
