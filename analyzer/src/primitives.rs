use std::fmt;

use fmt::Display;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum PrimitiveKind {
    Assert,
    Concat,
    Split,
    PwHash,
    Hash,
    Hkdf,
    AeadEnc,
    AeadDec,
    Enc,
    Dec,
    Mac,
    Sign,
    SignVerif,
    PkeEnc,
    PkeDec,
    ShamirSplit,
    ShamirJoin,
    RingSign,
    RingSignVerif,
    Blind,
    UnBlind,
    Exp,
}

impl PrimitiveKind {
    pub fn new(name: String) -> Self {
        match name.as_str() {
            "ASSERT" => PrimitiveKind::Assert,
            "CONCAT" => PrimitiveKind::Concat,
            "SPLIT" => PrimitiveKind::Split,
            "PW_HASH" => PrimitiveKind::PwHash,
            "HASH" => PrimitiveKind::Hash,
            "HKDF" => PrimitiveKind::Hkdf,
            "AEAD_ENC" => PrimitiveKind::AeadEnc,
            "AEAD_DEC" => PrimitiveKind::AeadDec,
            "ENC" => PrimitiveKind::Enc,
            "DEC" => PrimitiveKind::Dec,
            "MAC" => PrimitiveKind::Mac,
            "SIGN" => PrimitiveKind::Sign,
            "SIGNVERIF" => PrimitiveKind::SignVerif,
            "PKE_ENC" => PrimitiveKind::PkeEnc,
            "PKE_DEC" => PrimitiveKind::PkeDec,
            "SHAMIR_SPLIT" => PrimitiveKind::ShamirSplit,
            "SHAMIR_JOIN" => PrimitiveKind::ShamirJoin,
            "RINGSIGN" => PrimitiveKind::RingSign,
            "RINGSIGNVERIF" => PrimitiveKind::RingSignVerif,
            "BLIND" => PrimitiveKind::Blind,
            "UNBLIND" => PrimitiveKind::UnBlind,
            "^" => PrimitiveKind::Exp,
            _ => unimplemented!("Primitive {} is not implemented", name),
        }
    }
}

impl Display for PrimitiveKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: convert back to uppercase
        write!(f, "{:?}", self)
    }
}
