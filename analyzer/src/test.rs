use crate::{primitives::PrimitiveKind, queries::QueryKind};

use super::IndexedIdentsModel;
use indexmap::IndexSet;
use std::fs;

fn build_set(v: &[&str]) -> IndexSet<String> {
    v.iter().map(|s| s.to_string()).collect()
}

fn build_set_primitive(v: &[&str]) -> IndexSet<PrimitiveKind> {
    v.iter()
        .map(|s| s.to_string())
        .map(PrimitiveKind::new)
        .collect()
}

fn build_set_query(v: &[&str]) -> IndexSet<QueryKind> {
    v.iter()
        .map(|s| s.to_string())
        .map(QueryKind::new)
        .collect()
}

#[test]
fn index_signal_protocol() {
    let content = fs::read_to_string("../examples/transport-layer/firefox-sync.vp").unwrap();
    let model = parser::parse(&content).unwrap();
    let model = IndexedIdentsModel::new(model);
    let idents_pr = build_set(&["ComputerA", "ComputerB", "Server"]);
    let idents_const = build_set(&[
        "username",
        "pass",
        "salt",
        "data",
        "hashedPassword",
        "h1",
        "h2",
        "e1",
        "h3",
        "usernameb",
        "h1b",
        "h2b",
        "d1",
    ]);
    let idents_queries = build_set_query(&["confidentiality", "authentication"]);
    let idents_fns = build_set_primitive(&["HASH", "HKDF", "AEAD_ENC", "ASSERT", "AEAD_DEC"]);

    assert_eq!(model.lookup.idents_const, idents_const);
    assert_eq!(model.lookup.idents_fns, idents_fns);
    assert_eq!(model.lookup.idents_queries, idents_queries);
    assert_eq!(model.lookup.idents_pr, idents_pr);
}
