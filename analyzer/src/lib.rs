mod flatten;
mod flow;
mod ident_lookup;
mod index;
mod primitives;
mod queries;
mod rewriting;

pub use flatten::FlattenedModel;
pub use flow::FlowModel;
pub use index::IndexedIdentsModel;
pub use rewriting::RewritingAnalysis;

#[cfg(test)]
mod test;
