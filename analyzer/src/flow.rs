use crate::{
    flatten::*,
    ident_lookup::{IdentKind, IdentLookup},
};
use fxhash::FxBuildHasher;
use parser::ast::{self, Attacker, Qualifier};
use petgraph::visit::{IntoEdgeReferences, NodeIndexable, NodeRef};
use petgraph::{prelude::*, visit::IntoNodeReferences};
use std::collections::{HashMap, HashSet};
use std::fmt;

type FlowIdxLookup = HashMap<FlowNode, NodeIndex, FxBuildHasher>;
type FlowGraph = StableDiGraph<FlowNode, FlowEdge>;

pub struct FlowModel {
    pub lookup: IdentLookup,
    pub fn_calls: Vec<Function>,
    pub graph: FlowGraph,
    pub queries: Vec<Query>,
    pub attacker: Attacker,
    pub publics: HashSet<(NodeIndex, usize)>,
}

impl FlowModel {
    pub fn new(fl: FlattenedModel) -> Self {
        let gr = get_flow(&fl);
        let mut result = Self {
            lookup: fl.lookup,
            fn_calls: fl.fn_calls,
            graph: gr,
            attacker: fl.model.attacker,
            queries: fl.model.queries,
            publics: HashSet::default(),
        };
        result.split_multi_return_fns();
        result.condense_public();
        result.apply_leaks();
        result.edit_transmission_nodes();
        result.remove_bindings();
        result.remove_publics();
        result
    }

    pub fn get_dot(&self) -> String {
        let mut s = vec![
            "digraph dataflow {".to_string(),
            "rankdir=LR;".to_string(),
            "ranksep=4;".to_string(),
        ];
        for node in self.graph.node_references() {
            let i = self.graph.to_index(node.id());
            s.push(format!(
                "{}[label=\"{}\"];",
                i,
                node.weight().as_str(&self.lookup, &self.fn_calls)
            ));
        }
        for edge in self.graph.edge_references() {
            let fr = self.graph.to_index(edge.source());
            let to = self.graph.to_index(edge.target());
            s.push(format!("{} -> {} [label=\"{}\"];", fr, to, edge.weight()));
        }
        s.push("}".to_string());
        s.join("\n")
    }

    fn remove_bindings(&mut self) {
        let mut new_edges = Vec::new();
        let mut intermediates = Vec::new();
        for node in self.graph.node_references() {
            if let FlowNode::Intermediate(_, _) = node.weight() {
                // Collect outgoing edge weights
                let outgoing: Vec<_> = self
                    .graph
                    .edges_directed(node.id(), Direction::Outgoing)
                    .map(|w| {
                        let (_, end) = self.graph.edge_endpoints(w.id()).unwrap();
                        (end, *w.weight())
                    })
                    .collect();

                // Get incoming neighbor node
                let mut incoming_node_iter = self
                    .graph
                    .neighbors_directed(node.id(), Direction::Incoming);
                let incoming = incoming_node_iter.next().unwrap();
                // No further incoming neighbors
                assert!(incoming_node_iter.next().is_none());

                // Add new edge between incoming and outgoing neighbor into todo
                // list since we can't manipulate the graph right now, since it
                // would shift indices. (Also the borrow checker would complain.)
                intermediates.push(node.id());
                new_edges.push((incoming, outgoing));
            }
        }

        // Add edges before removing intermediate nodes. (Node removal shifts
        // node indices.)
        for (a, bs) in new_edges {
            for (node, weight) in bs {
                self.graph.add_edge(a, node, weight);
            }
        }

        // Finally delete all intermediate nodes from the graph.
        for interm in intermediates {
            self.graph.remove_node(interm);
        }
    }

    fn remove_publics(&mut self) {
        let mut new_edges = Vec::new();
        let mut publics = Vec::new();
        for node in self.graph.node_references() {
            if let FlowNode::Public(id) = node.weight() {
                // Collect outgoing edge weights
                let outgoing: Vec<_> = self
                    .graph
                    .edges_directed(node.id(), Direction::Outgoing)
                    .map(|w| {
                        let (_, end) = self.graph.edge_endpoints(w.id()).unwrap();
                        (end, *w.weight())
                    })
                    .collect();

                // Get incoming neighbor node
                let mut incoming_node_iter = self
                    .graph
                    .neighbors_directed(node.id(), Direction::Incoming);
                let incoming = incoming_node_iter.next().unwrap();
                // No further incoming neighbors
                assert!(incoming_node_iter.next().is_none());

                // Add new edge between incoming and outgoing neighbor into todo
                // list since we can't manipulate the graph right now, since it
                // would shift indices. (Also the borrow checker would complain.)
                publics.push(node.id());

                // Mark incoming node as public.
                self.publics.insert((incoming, *id));

                new_edges.push((incoming, outgoing));
            }
        }

        // Add edges before removing intermediate nodes. (Node removal shifts
        // node indices.)
        for (a, bs) in new_edges {
            for (node, weight) in bs {
                self.graph.add_edge(a, node, weight);
            }
        }

        // Finally delete all public nodes from the graph.
        for p in publics {
            self.graph.remove_node(p);
        }
    }

    fn condense_public(&mut self) {
        let mut join_map = HashMap::new();
        let mut to_be_removed = Vec::new();
        for (node_id, node_weight) in self.graph.node_references() {
            if let FlowNode::Public(n) = node_weight {
                if join_map.contains_key(n) {
                    to_be_removed.push((*n, node_id));
                } else {
                    join_map.insert(*n, node_id);
                }
            }
        }

        let mut new_edges = Vec::new();
        for (n, node_id) in &to_be_removed {
            let main_node = join_map[n];
            for edge in self.graph.edges_directed(*node_id, Outgoing) {
                let (_, e) = self.graph.edge_endpoints(edge.id()).unwrap();
                new_edges.push((main_node, e, *edge.weight()));
            }
        }

        for (s, e, w) in new_edges {
            self.graph.add_edge(s, e, w);
        }

        for (_, node_id) in to_be_removed {
            self.graph.remove_node(node_id);
        }
    }

    fn split_multi_return_fns(&mut self) {
        let mut edges_with_first_out = Vec::new();
        let mut other_edges = Vec::new();
        for edge in self.graph.edge_references() {
            if let FlowEdge::FunctionOutput(i) = edge.weight() {
                if *i == 0 {
                    edges_with_first_out.push(edge.id());
                } else {
                    other_edges.push((*i, edge.id()));
                }
            }
        }

        for first_out in edges_with_first_out {
            let (s, _) = self.graph.edge_endpoints(first_out).unwrap();
            let node_weight = self.graph.node_weight(s).unwrap();
            let principal = node_weight.get_principal().unwrap();
            let function = node_weight.get_function();
            let node_weight = self.graph.node_weight_mut(s).unwrap();
            *node_weight = FlowNode::FunctionSingleReturn(principal, function, 0);
        }

        for (out_idx, edge) in other_edges {
            let (start, end) = self.graph.edge_endpoints(edge).unwrap();
            let start_node_weight = self.graph.node_weight(start).unwrap();
            let principal = start_node_weight.get_principal().unwrap();
            let function = start_node_weight.get_function();
            let new_start_node = FlowNode::FunctionSingleReturn(principal, function, out_idx);
            let new_start_idx = self.graph.add_node(new_start_node);
            self.graph
                .add_edge(new_start_idx, end, FlowEdge::FunctionOutput(0));
            let mut parent_edges = Vec::new();
            for parent_edge in self.graph.edges_directed(start, Incoming) {
                let (p_start, _) = self.graph.edge_endpoints(parent_edge.id()).unwrap();
                parent_edges.push((p_start, *parent_edge.weight()));
            }
            for (parent_parent_node, weight) in parent_edges {
                self.graph
                    .add_edge(parent_parent_node, new_start_idx, weight);
            }

            self.graph.remove_edge(edge).unwrap();
        }
    }

    fn apply_leaks(&mut self) {
        let mut leaks = Vec::new();
        for (node_id, node_weight) in self.graph.node_references() {
            if let FlowNode::Leaks(_, _) = node_weight {
                leaks.push(node_id);
            }
        }

        let mut target_nodes = Vec::new();
        for node_id in &leaks {
            for consts in self.graph.neighbors_directed(*node_id, Incoming) {
                target_nodes.push(consts);
            }
        }
        for target in target_nodes {
            let weight = self.graph.node_weight_mut(target).unwrap();
            let n = match weight {
                FlowNode::Private(_, n) => n,
                FlowNode::Binding(_, n) => n,
                FlowNode::Public(n) => n,
                _ => unreachable!("found {:?}", weight),
            };
            *weight = FlowNode::Public(*n);
        }

        for n in leaks {
            self.graph.remove_node(n);
        }
    }

    fn edit_transmission_nodes(&mut self) {
        let mut public = Vec::new();
        let mut new_free = Vec::new();
        for edge in self.graph.edge_references() {
            let (start, end) = self.graph.edge_endpoints(edge.id()).unwrap();
            match edge.weight() {
                FlowEdge::TransmissionGuarded => {
                    public.push(start);
                }
                FlowEdge::TransmissionUnguarded => {
                    public.push(start);
                    new_free.push(end);
                }
                _ => (),
            }
        }

        for target in public {
            let weight = self.graph.node_weight_mut(target).unwrap();
            let n = match weight {
                FlowNode::Private(_, n) => n,
                FlowNode::Binding(_, n) => n,
                FlowNode::Public(n) => n,
                _ => unreachable!("found {:?}", weight),
            };
            *weight = FlowNode::Public(*n);
        }

        for target in new_free {
            let weight = self.graph.node_weight_mut(target).unwrap();
            let n = match weight {
                FlowNode::Private(_, n) => n,
                FlowNode::Binding(_, n) => n,
                FlowNode::Public(n) => n,
                _ => unreachable!("found {:?}", weight),
            };
            *weight = FlowNode::Free(*n);
        }
    }
}

#[derive(Debug, Clone, Copy, Hash)]
pub enum FlowEdge {
    TransmissionGuarded,
    TransmissionUnguarded,
    InternalAssignment,
    FunctionOutput(usize),
    FunctionParameter(usize),
}

impl FlowEdge {
    pub fn is_unguarded(&self) -> bool {
        matches!(self, FlowEdge::TransmissionUnguarded)
    }

    pub fn get_fn_arg_idx(&self) -> usize {
        match self {
            FlowEdge::FunctionParameter(x) => *x,
            _ => unreachable!(),
        }
    }
}

impl fmt::Display for FlowEdge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            FlowEdge::TransmissionGuarded => write!(f, "g"),
            FlowEdge::TransmissionUnguarded => write!(f, "ug"),
            FlowEdge::InternalAssignment => write!(f, "i"),
            FlowEdge::FunctionParameter(i) => write!(f, "{}", i),
            FlowEdge::FunctionOutput(i) => write!(f, "{}", i),
        }
    }
}

// First variable is the principal, second variable is the constant
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum FlowNode {
    Dump,
    Nil,
    Generator,
    Function(usize, usize),
    FunctionSingleReturn(usize, usize, usize),
    Intermediate(usize, usize),
    Binding(usize, usize),
    Generate(usize, usize),
    Knows(usize, usize, ast::Qualifier),
    Leaks(usize, usize),
    Public(usize),
    Free(usize),
    Private(usize, usize),
}

impl FlowNode {
    pub fn as_str(&self, lookup: &IdentLookup, fn_call: &[Function]) -> String {
        match self {
            FlowNode::Function(p, x) => {
                let fun = &fn_call[*x];
                format!(
                    "{}\n{}(...)",
                    lookup.get_string(IdentKind::Principal, *p),
                    lookup.get_string(IdentKind::Function, fun.name)
                )
            }
            FlowNode::FunctionSingleReturn(p, x, out) => {
                let fun = &fn_call[*x];
                format!(
                    "{}\n{}(...)\nOut {}",
                    lookup.get_string(IdentKind::Principal, *p),
                    lookup.get_string(IdentKind::Function, fun.name),
                    out
                )
            }
            FlowNode::Intermediate(p, x) => format!(
                "{}\nIntermediate {}",
                lookup.get_string(IdentKind::Principal, *p),
                x
            ),
            FlowNode::Binding(p, x) => format!(
                "{}\nConstant {}",
                lookup.get_string(IdentKind::Principal, *p),
                lookup.get_string(IdentKind::Constant, *x)
            ),
            FlowNode::Generate(p, x) => format!(
                "{}\nGenerate {}",
                lookup.get_string(IdentKind::Principal, *p),
                lookup.get_string(IdentKind::Constant, *x)
            ),
            FlowNode::Knows(p, x, q) => match q {
                Qualifier::Private | Qualifier::Password => {
                    format!(
                        "{}\nKnows {:?}\n{}",
                        lookup.get_string(IdentKind::Principal, *p),
                        q,
                        lookup.get_string(IdentKind::Constant, *x)
                    )
                }
                Qualifier::Public => {
                    format!("Public {}", lookup.get_string(IdentKind::Constant, *x))
                }
            },
            FlowNode::Leaks(p, x) => format!(
                "{}\nLeaks {}",
                lookup.get_string(IdentKind::Principal, *p),
                lookup.get_string(IdentKind::Constant, *x)
            ),
            FlowNode::Public(x) => format!("Public {}", lookup.get_string(IdentKind::Constant, *x)),
            FlowNode::Free(x) => format!("Free {}", lookup.get_string(IdentKind::Constant, *x)),
            FlowNode::Private(p, x) => format!(
                "{}\nPrivate {}",
                lookup.get_string(IdentKind::Principal, *p),
                lookup.get_string(IdentKind::Constant, *x)
            ),
            FlowNode::Nil => "Nil".to_string(),
            FlowNode::Generator => "G".to_string(),
            FlowNode::Dump => "_".to_string(),
        }
    }

    fn get_principal(&self) -> Option<usize> {
        match self {
            FlowNode::Binding(n, _)
            | FlowNode::Function(n, _)
            | FlowNode::FunctionSingleReturn(n, _, _)
            | FlowNode::Generate(n, _)
            | FlowNode::Intermediate(n, _)
            | FlowNode::Private(n, _)
            | FlowNode::Knows(n, _, _)
            | FlowNode::Leaks(n, _) => Some(*n),
            _ => None,
        }
    }

    fn get_function(&self) -> usize {
        match self {
            FlowNode::Function(_, n) => *n,
            FlowNode::FunctionSingleReturn(_, n, _) => *n,
            _ => unreachable!("{:?}", self),
        }
    }
}

fn get_flow(fl: &FlattenedModel) -> FlowGraph {
    let mut gr = FlowGraph::default();
    let mut idx = FlowIdxLookup::default();
    for b in &fl.model.blocks {
        match b {
            Block::Phase(_) => {}
            Block::Principal(p) => get_flow_pr(p, fl, &mut gr, &mut idx),
            Block::Transmission(t) => get_flow_tr(t, &mut gr, &mut idx),
        }
    }
    gr
}

fn get_node_rvalue(
    p: &Principal,
    rv: &Value,
    fl: &FlattenedModel,
    gr: &mut FlowGraph,
    idx: &mut FlowIdxLookup,
) -> FlowNode {
    match rv {
        Value::Declared(c) => FlowNode::Binding(p.name, *c),
        Value::Intermediate(c) => FlowNode::Intermediate(p.name, *c),
        Value::Function(f) => {
            let fnode = FlowNode::Function(p.name, *f);
            let fun = &fl.fn_calls[*f];
            for (i, param) in fun.parameters.iter().enumerate() {
                let c = match param {
                    Value::Intermediate(c) => FlowNode::Intermediate(p.name, *c),
                    Value::Declared(c) => FlowNode::Binding(p.name, *c),
                    Value::Function(_) => unreachable!(),
                    Value::Nil => FlowNode::Nil,
                    Value::Generator => FlowNode::Generator,
                    Value::Dump => FlowNode::Dump,
                };
                add_edge_to_gr(c, fnode, FlowEdge::FunctionParameter(i), gr, idx);
            }
            fnode
        }
        Value::Nil => FlowNode::Nil,
        Value::Generator => FlowNode::Generator,
        Value::Dump => FlowNode::Dump,
    }
}

#[allow(clippy::map_entry)]
fn add_node_to_gr(node: FlowNode, gr: &mut FlowGraph, idx: &mut FlowIdxLookup) -> NodeIndex {
    if idx.contains_key(&node) {
        idx[&node]
    } else {
        let node_idx = gr.add_node(node);
        idx.insert(node, node_idx);
        node_idx
    }
}

fn add_edge_to_gr(
    from: FlowNode,
    to: FlowNode,
    edge: FlowEdge,
    gr: &mut FlowGraph,
    idx: &mut FlowIdxLookup,
) {
    let source = add_node_to_gr(from, gr, idx);
    let target = add_node_to_gr(to, gr, idx);
    gr.add_edge(source, target, edge);
}

fn get_flow_pr(pr: &Principal, fl: &FlattenedModel, gr: &mut FlowGraph, idx: &mut FlowIdxLookup) {
    for expr in &pr.expressions {
        match expr {
            Expression::Assignment(constants, rvalue) => {
                let r = get_node_rvalue(pr, rvalue, fl, gr, idx);
                for (out_idx, constant) in constants.iter().enumerate() {
                    let l = match constant {
                        Value::Intermediate(c) => FlowNode::Intermediate(pr.name, *c),
                        Value::Declared(c) => FlowNode::Binding(pr.name, *c),
                        Value::Function(_) => unreachable!(),
                        Value::Nil => FlowNode::Nil,
                        Value::Generator => FlowNode::Generator,
                        Value::Dump => FlowNode::Dump,
                    };
                    add_edge_to_gr(r, l, FlowEdge::FunctionOutput(out_idx), gr, idx);
                }
            }
            Expression::Generated(x) => {
                let gen = FlowNode::Generate(pr.name, *x);
                let con = FlowNode::Binding(pr.name, *x);
                add_edge_to_gr(gen, con, FlowEdge::InternalAssignment, gr, idx);
            }
            Expression::Knows(x, q) => {
                let kno = FlowNode::Knows(pr.name, *x, *q);
                let con = FlowNode::Binding(pr.name, *x);
                add_edge_to_gr(kno, con, FlowEdge::InternalAssignment, gr, idx);
            }
            Expression::Leaks(x) => {
                let lk = FlowNode::Leaks(pr.name, *x);
                let con = FlowNode::Binding(pr.name, *x);
                add_edge_to_gr(con, lk, FlowEdge::InternalAssignment, gr, idx);
            }
        };
    }
}

fn get_idx_package(p: ast::Package<usize>) -> usize {
    match p {
        ast::Package::Unguarded(t) => t,
        ast::Package::Guarded(t) => t,
    }
}

fn get_guard_package(p: ast::Package<usize>) -> FlowEdge {
    match p {
        ast::Package::Unguarded(_) => FlowEdge::TransmissionUnguarded,
        ast::Package::Guarded(_) => FlowEdge::TransmissionGuarded,
    }
}

fn get_flow_tr(t: &Transmission, gr: &mut FlowGraph, idx: &mut FlowIdxLookup) {
    let send_c = FlowNode::Binding(t.sender, get_idx_package(t.package));
    let recv_c = FlowNode::Binding(t.receiver, get_idx_package(t.package));

    add_edge_to_gr(send_c, recv_c, get_guard_package(t.package), gr, idx);
}
