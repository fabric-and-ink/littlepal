use crate::ident_lookup::{IdentKind, IdentLookup};
use parser::ast::*;

fn replace_ident_package(c: Package<String>, lookup: &mut IdentLookup) -> Package<usize> {
    match c {
        Package::Unguarded(s) => Package::Unguarded(lookup.insert(IdentKind::Constant, s)),
        Package::Guarded(s) => Package::Guarded(lookup.insert(IdentKind::Constant, s)),
    }
}

fn replace_ident_message(m: Message<String>, lookup: &mut IdentLookup) -> Message<usize> {
    Message {
        sender: lookup.insert(IdentKind::Principal, m.sender),
        receiver: lookup.insert(IdentKind::Principal, m.receiver),
        packages: m
            .packages
            .into_iter()
            .map(|p| replace_ident_package(p, lookup))
            .collect(),
    }
}

fn replace_ident_rvalue(v: RValue<String>, lookup: &mut IdentLookup) -> RValue<usize> {
    match v {
        RValue::Generator => RValue::Generator,
        RValue::Nil => RValue::Nil,
        RValue::Constant(s) => RValue::Constant(lookup.insert(IdentKind::Constant, s)),
        RValue::Function(f) => RValue::Function(replace_ident_fn(f, lookup)),
    }
}

fn replace_ident_fn(f: Function<String>, lookup: &mut IdentLookup) -> Function<usize> {
    Function {
        name: lookup.insert(IdentKind::Function, f.name),
        parameters: f
            .parameters
            .into_iter()
            .map(|p| replace_ident_rvalue(p, lookup))
            .collect(),
        check: f.check,
    }
}

fn replace_ident_lvalue(l: LValue<String>, lookup: &mut IdentLookup) -> LValue<usize> {
    match l {
        LValue::Dump => LValue::Dump,
        LValue::Constant(c) => LValue::Constant(lookup.insert(IdentKind::Constant, c)),
    }
}

fn replace_ident_expr(e: Expression<String>, lookup: &mut IdentLookup) -> Expression<usize> {
    match e {
        Expression::Assignment(i, v) => Expression::Assignment(
            i.into_iter()
                .map(|i| replace_ident_lvalue(i, lookup))
                .collect(),
            replace_ident_rvalue(v, lookup),
        ),
        Expression::Generates(i) => {
            Expression::Generates(lookup.insert_many(IdentKind::Constant, i))
        }
        Expression::Knows(i, q) => Expression::Knows(lookup.insert_many(IdentKind::Constant, i), q),
        Expression::Leaks(i) => Expression::Leaks(lookup.insert_many(IdentKind::Constant, i)),
    }
}

fn replace_ident_principal(p: Principal<String>, lookup: &mut IdentLookup) -> Principal<usize> {
    Principal {
        name: lookup.insert(IdentKind::Principal, p.name),
        expressions: p
            .expressions
            .into_iter()
            .map(|e| replace_ident_expr(e, lookup))
            .collect(),
    }
}

fn replace_ident_block(b: Block<String>, lookup: &mut IdentLookup) -> Block<usize> {
    match b {
        Block::Message(m) => Block::Message(replace_ident_message(m, lookup)),
        Block::Phase(p) => Block::Phase(p),
        Block::Principal(p) => Block::Principal(replace_ident_principal(p, lookup)),
    }
}

fn replace_ident_query_target(
    qt: QueryTarget<String>,
    lookup: &mut IdentLookup,
) -> QueryTarget<usize> {
    match qt {
        QueryTarget::Constants(c) => {
            QueryTarget::Constants(lookup.insert_many(IdentKind::Constant, c))
        }
        QueryTarget::Message(m) => QueryTarget::Message(replace_ident_message(m, lookup)),
    }
}

fn replace_ident_query(q: Query<String>, lookup: &mut IdentLookup) -> Query<usize> {
    let if_true_msg = q.if_true_msg.map(|m| replace_ident_message(m, lookup));
    Query::new(
        lookup.insert(IdentKind::Query, q.name),
        q.inverted,
        replace_ident_query_target(q.target, lookup),
        if_true_msg,
    )
}

fn replace_ident_model(m: Model<String>, lookup: &mut IdentLookup) -> Model<usize> {
    Model::new(
        m.attacker,
        m.blocks
            .into_iter()
            .map(|b| replace_ident_block(b, lookup))
            .collect(),
        m.queries
            .into_iter()
            .map(|q| replace_ident_query(q, lookup))
            .collect(),
    )
}

#[derive(Debug)]
pub struct IndexedIdentsModel {
    pub lookup: IdentLookup,
    pub model: Model<usize>,
}

impl IndexedIdentsModel {
    pub fn new(m: Model<String>) -> Self {
        let mut lookup = IdentLookup::new();

        let model = replace_ident_model(m, &mut lookup);

        Self { lookup, model }
    }
}
