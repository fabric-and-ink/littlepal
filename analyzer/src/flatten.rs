use crate::{ident_lookup::IdentLookup, index::IndexedIdentsModel};
use ast::Attacker;
use parser::ast;

#[derive(Debug)]
pub struct FlattenedModel {
    pub lookup: IdentLookup,
    pub fn_calls: Vec<Function>,
    pub model: Model,
}

impl FlattenedModel {
    pub fn new(m: IndexedIdentsModel) -> Self {
        let mut idx = IdxProvider::new();
        let mut fn_calls = Vec::new();
        let model = decompose_model(m.model, &mut idx, &mut fn_calls);
        Self {
            lookup: m.lookup,
            fn_calls,
            model,
        }
    }
}

#[derive(Debug)]
pub struct Model {
    pub attacker: Attacker,
    pub blocks: Vec<Block>,
    pub queries: Vec<Query>,
}

impl Model {
    fn new(attacker: Attacker, blocks: Vec<Block>, queries: Vec<Query>) -> Self {
        Self {
            attacker,
            blocks,
            queries,
        }
    }
}

#[derive(Debug)]
pub enum Block {
    Phase(ast::Phase),
    Principal(Principal),
    Transmission(Transmission),
}

#[derive(Debug)]
pub struct Principal {
    pub name: usize,
    pub expressions: Vec<Expression>,
}

impl Principal {
    fn new(name: usize, expressions: Vec<Expression>) -> Self {
        Self { name, expressions }
    }
}

#[derive(Debug)]
pub struct Function {
    pub name: usize,
    pub parameters: Vec<Value>,
    pub check: bool,
}

impl Function {
    pub fn new(name: usize, parameters: Vec<Value>, check: bool) -> Self {
        Self {
            name,
            parameters,
            check,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Value {
    Dump,
    Nil,
    Generator,
    Intermediate(usize),
    Declared(usize),
    Function(usize),
}

impl From<ast::LValue<usize>> for Value {
    fn from(l: ast::LValue<usize>) -> Self {
        match l {
            ast::LValue::Dump => Value::Dump,
            ast::LValue::Constant(c) => Value::Declared(c),
        }
    }
}

#[derive(Debug)]
pub enum Expression {
    Assignment(Vec<Value>, Value),
    Generated(usize),
    Knows(usize, ast::Qualifier),
    Leaks(usize),
}

#[derive(Debug)]
pub struct Transmission {
    pub sender: usize,
    pub receiver: usize,
    pub package: ast::Package<usize>,
}

impl Transmission {
    pub fn new(sender: usize, receiver: usize, package: ast::Package<usize>) -> Self {
        Self {
            sender,
            receiver,
            package,
        }
    }
}

#[derive(Debug)]
pub enum QueryTarget {
    Constants(Vec<usize>),
    Transmission(Transmission),
}

#[derive(Debug)]
pub struct Query {
    pub name: usize,
    pub inverted: bool,
    pub target: QueryTarget,
    pub if_true_msg: Option<Vec<Transmission>>,
}

impl Query {
    pub fn new(
        name: usize,
        inverted: bool,
        target: QueryTarget,
        if_true_msg: Option<Vec<Transmission>>,
    ) -> Self {
        Self {
            name,
            inverted,
            target,
            if_true_msg,
        }
    }

    pub fn get_tr_const_id(&self) -> usize {
        match &self.target {
            QueryTarget::Transmission(t) => match t.package {
                ast::Package::Unguarded(c) => c,
                ast::Package::Guarded(c) => c,
            },
            _ => panic!("The query is not a transmission-related query"),
        }
    }

    pub fn get_consts_id(&self) -> &[usize] {
        match &self.target {
            QueryTarget::Constants(t) => t,
            _ => panic!("The query is not a const-related query"),
        }
    }
}

struct IdxProvider {
    counter: usize,
}

impl IdxProvider {
    fn new() -> Self {
        Self { counter: 0 }
    }

    fn get(&mut self) -> usize {
        self.counter += 1;
        self.counter - 1
    }
}

fn decompose_function(
    f: ast::Function<usize>,
    idx: &mut IdxProvider,
    exp: &mut Vec<Expression>,
    fn_calls: &mut Vec<Function>,
) -> usize {
    let newp: Vec<Value> = f
        .parameters
        .into_iter()
        .map(|p| match p {
            ast::RValue::Constant(c) => Value::Declared(c),
            ast::RValue::Function(f) => {
                let subf = decompose_function(f, idx, exp, fn_calls);
                let i = Value::Intermediate(idx.get());
                exp.push(Expression::Assignment(vec![i], Value::Function(subf)));
                i
            }
            ast::RValue::Nil => Value::Nil,
            ast::RValue::Generator => Value::Generator,
        })
        .collect();

    fn_calls.push(Function::new(f.name, newp, f.check));
    fn_calls.len() - 1
}

fn decompose_rvalue(
    v: ast::RValue<usize>,
    idx: &mut IdxProvider,
    exp: &mut Vec<Expression>,
    fn_calls: &mut Vec<Function>,
) -> Value {
    match v {
        ast::RValue::Constant(c) => Value::Declared(c),
        ast::RValue::Function(f) => Value::Function(decompose_function(f, idx, exp, fn_calls)),
        ast::RValue::Nil => Value::Nil,
        ast::RValue::Generator => Value::Generator,
    }
}

fn decompose_expression(
    f: ast::Expression<usize>,
    idx: &mut IdxProvider,
    exp: &mut Vec<Expression>,
    fn_calls: &mut Vec<Function>,
) -> Vec<Expression> {
    match f {
        ast::Expression::Assignment(l, r) => {
            let r = decompose_rvalue(r, idx, exp, fn_calls);
            vec![Expression::Assignment(
                l.into_iter().map(|i| i.into()).collect(),
                r,
            )]
        }
        ast::Expression::Generates(a) => a.into_iter().map(Expression::Generated).collect(),
        ast::Expression::Knows(a, q) => a.into_iter().map(|a| Expression::Knows(a, q)).collect(),
        ast::Expression::Leaks(a) => a.into_iter().map(Expression::Leaks).collect(),
    }
}

fn decompose_message(m: ast::Message<usize>) -> Vec<Transmission> {
    m.packages
        .iter()
        .map(|p| Transmission::new(m.sender, m.receiver, *p))
        .collect()
}

fn decompose_query(q: ast::Query<usize>) -> Vec<Query> {
    #[allow(clippy::redundant_closure)]
    match q.target {
        ast::QueryTarget::Constants(l) => {
            vec![Query::new(
                q.name,
                q.inverted,
                QueryTarget::Constants(l),
                q.if_true_msg.clone().map(|m| decompose_message(m)),
            )]
        }
        ast::QueryTarget::Message(ref m) => m
            .packages
            .iter()
            .map(|p| {
                Query::new(
                    q.name,
                    q.inverted,
                    QueryTarget::Transmission(Transmission::new(m.sender, m.receiver, *p)),
                    q.if_true_msg.clone().map(|m| decompose_message(m)),
                )
            })
            .collect(),
    }
}

fn decompose_principal(
    p: ast::Principal<usize>,
    idx: &mut IdxProvider,
    fn_calls: &mut Vec<Function>,
) -> Principal {
    let mut new_exp = Vec::new();

    // Collecting the intermediate result is required since new_exp is extended
    // during mapping.
    #[allow(clippy::needless_collect)]
    let translated_exp: Vec<_> = p
        .expressions
        .into_iter()
        .flat_map(|e| decompose_expression(e, idx, &mut new_exp, fn_calls))
        .collect();

    new_exp.extend(translated_exp.into_iter());
    Principal::new(p.name, new_exp)
}

fn decompose_block(
    b: ast::Block<usize>,
    idx: &mut IdxProvider,
    fn_calls: &mut Vec<Function>,
) -> Vec<Block> {
    match b {
        ast::Block::Message(m) => decompose_message(m)
            .into_iter()
            .map(Block::Transmission)
            .collect(),
        ast::Block::Phase(p) => vec![Block::Phase(p)],
        ast::Block::Principal(p) => {
            vec![Block::Principal(decompose_principal(p, idx, fn_calls))]
        }
    }
}

fn decompose_model(
    m: ast::Model<usize>,
    idx: &mut IdxProvider,
    fn_calls: &mut Vec<Function>,
) -> Model {
    Model::new(
        m.attacker,
        m.blocks
            .into_iter()
            .flat_map(|b| decompose_block(b, idx, fn_calls))
            .collect(),
        m.queries.into_iter().flat_map(decompose_query).collect(),
    )
}
