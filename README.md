# littlepal :frog:

littlepal parses [Verifpal][verifpal] protocol models and analyses them. The
main motivation for writing littlepal is to learn something. However, there is
hope to improve the analysis speed as well, since there are cases were Verifpal
analysis time explodes. This is a fundamental issue but I am optimistic that the
envelope can be pushed some more. Also a second implementation will be helpful
in cross-checking the solvers.

## Internal Processing Steps

littlepal currently performs the following steps.

1. Read the model into an AST.
2. Replace identifiers with indices into name tables.
3. Decompose compound expressions into simple expressions. E.g. one transmission
   that sends several messages from Alice to Bob is broken into many
   transmission with one message each. Or nested function calls get separated
   where the results are assigned to intermediate constants. This helps to
   tackle complex models and makes it easier to pin more fine grained
   information to the decomposed parts. Some of this decomposition is reverted
   in later steps however, e.g. intermediate constants get removed again.
4. Build up a data flow graph of the model. Initially, constants and functions
   are added as nodes. Edges are labelled with "internal" or "external" (guarded
   or unguarded) transmissions. Then, the graph is cleaned up so that no
   constants (apart from information sources like generators or known constants)
   remain. Some more cleanup is going on, but that is not as important here.
   This step can also export some nice data flow graphs.
5. The graph is converted into a equivalence graph structure and rewriting rules
   are applied iteratively until the graph is saturated. More details on this
   step below.

## Rewriting

The idea for rewriting the expressions in the model is similar to how it is
being done in Verifpal itself. However, littlepal tries to take it one step
further by encoding the guarded and unguarded transmissions into the rewriting
rules. (But fails to do so at the moment.) Internally, littlepal uses the `egg`
rewriting engine for fast rewriting of expressions. It uses a very efficient
representation of equivalence classes and provides fast pattern matching. If one
expression can be rewritten into to another, both are in the same equivalence
class. The rewriting rules are applied until the graph is saturated, i.e. no new
expressions can be found by applying the rules.

The rewriting rules are expressed in a lisp-like notation. For example, the
expression

```text
(dec key (enc key msg))
```

where `(enc key msg)` denotes the encryption of a message `msg` with the key
`key` and `(dec key c)` denotes a decryption with the key `key` of the
ciphertext `c` can be rewritten to `msg`. In this text, the rewriting rule is
denoted as follows.

```text
(dec key (enc key msg))
-----------------------
msg
```

Since in protocols information has to be exchanged over open channels, an
attacker will be able to observe the exchanged information and possibly inject
new messages into these channels. Let's say that an attacker has access to the
following information. Here, `guard` and `unguard` marks information that is
transmitted over an public guarded or unguarded channel respectively.

```text
(guard key)
(guard (enc key msg))
```

Then, since the message is encrypted with a key that is transmitted openly, an
attacker can combine both pieces of information to decrypt the message.
Therefore, a rule as follows can be established.

```text
(guard key) (guard (enc key msg))
---------------------------------
(guard msg)
```

Unguarded transmissions become interesting if the attacker is able to substitute
a key that is used to encrypt a confidential message. For example, if Alice
encrypts a message with a key that in some way was substituted by the attacker
and Alice sends the encrypted message over an open channel, the attacker will be
able to decrypt it.

```text
(guard (enc (unguard key) msg))
-------------------------------
(guard msg)
```

## Example

For the technical background of the equivalence graph (e-graph) theory, refer to
[the `egg` paper][egg-paper]. Note that the main use case for `egg` goes beyond
simple rewriting to optimizing expressions according to a certain metric.

As an example, take the following model.

```text
principal Alice[
   generates a
   ga = G^a
]

Alice -> Bob: ga

principal Bob[
   generates m1, b
   gb = G^b
   gab = ga^b
   e1 = ENC(gab, m1)
]

Bob -> Alice: gb, e1

principal Alice[
   gba = gb^a
   e1_dec = DEC(ga, e1)
]
```

This is translated into the following graph.

```text
0:  [Sym("a")]
1:  [Sym("#g")]
2:  [Sym("ga"), UnGuard(10)]
3:  [Sym("m1")]
4:  [Sym("b")]
5:  [Sym("gab"), Exp([2, 4])]
6:  [Sym("gb"), UnGuard(11)]
7:  [Sym("e1"), UnGuard(13)]
10: [Exp([1, 0])]
11: [Exp([1, 4])]
13: [Enc([5, 3])]
14: [Sym("gba"), Exp([6, 0])]
15: [Sym("e1_dec"), Dec([10, 7])]
16: [Pub(10)]
18: [Pub(11)]
20: [Pub(13)]
```

Here, symbols with names can be seen, like `Sym("a")`, etc. Furthermore there
are expressions like `Exp([6, 0])` that refer to indices, which are shown on the
left. Each row is an equivalence class, i.e. all entries are regarded as
identical. After applying the current set of rewriting rules the graph changes
into the following.

```text
0:  [Sym("a")]
1:  [Sym("#g")]
2:  [Sym("ga"), UnGuard(10)]
3:  [Sym("m1")]
4:  [Sym("b")]
5:  [Sym("gab"), Sym("gba"), UnGuard(22), Exp([2, 4]), Exp([6, 0])]
6:  [Sym("gb"), UnGuard(11)]
7:  [Sym("e1"), UnGuard(13)]
10: [Exp([1, 0])]
11: [Exp([1, 4])]
13: [Enc([5, 3])]
15: [Sym("e1_dec"), Dec([10, 7])]
16: [Pub(10)]
18: [Pub(11)]
22: [Exp([10, 4]), Exp([11, 0])]
26: [Pub(3), Pub(13)]
```

Some observations:

- `gab` and `gba` are identical.
- `m1` is referred to by e-class 26, which indicates that it is public
  information.

## Open Questions and Tasks

These are some general open guestions and I would like to involve others at this
point, since I am struggling since several months to make significant progress.
Some attempts at answering the open questions already exist in the code but they
do not yet lead to consistent results.

0. Does this idea even work in theory?
1. How to set up the initial equivalence graph before any rewriting rule is
   applied?
    - The rewriting rules can incorporate much more information than shown
      above. Arbitrary information can be carried "in the background" for each
      equivalence class that can be used to decide which rewriting rule to
      apply.
    - The data flow graph preprocessing step allows very fine grained
      configuration of the amount of information that is forwarded to the
      equivalence graph step. What information is needed? What can be deleted?
      Is it necessary to derive more information in the data flow graph before
      continuing?
2. Which rules to apply? (The current rule set is [here][rules]. Note that it is
   wrong and inconsistent. These rules only give correct results for a small
   subset of the problem space. The models that currently are checked correctly
   can be found [here][tests] in the subfolders beginning with "simple".)
    - Is it even possible to capture the semantics of Verifpal's model of crypto
      protocols in this way? (I am optimistic here.)
    - Are different set of rules required for different queries? Or can one set
      of rules answer all current queries.
3. How to implement the queries?
    - The equivalence query is simple: "are these two things in the same
      equivalence class?".
    - The confidentiality query checks if a symbol is public: "For a symbol X,
      is there an expression that has the pattern `(Pub X)`?" (`Pub` was not
      mentioned above but it currently denotes all things that are publicly
      known in some way or other.)

[verifpal]: https://verifpal.com/
[egg-paper]: https://dl.acm.org/doi/10.1145/3434304
[rules]: https://gitlab.com/fabric-and-ink/littlepal/-/blob/2b44b092bc9afa02cd8ca7afc4656fd89ae8118f/analyzer/src/rewriting.rs#L362-510
[tests]: https://gitlab.com/fabric-and-ink/littlepal/-/tree/main/test_input

## Usage

```
littlepal -f {filename}
```

Currently this will dump the equivalence graph of the input model and a data
flow graph. Some first bits of a query resolver are there, but don't trust it. I
don't.

## Binaries
- [linux](https://gitlab.com/fabric-and-ink/littlepal/-/jobs/artifacts/master/raw/bin/littlepal?job=build_linux)
- [win](https://gitlab.com/fabric-and-ink/littlepal/-/jobs/artifacts/master/raw/bin/littlepal.exe?job=build_win)
- [macos (x64)](https://gitlab.com/fabric-and-ink/littlepal/-/jobs/artifacts/master/raw/bin/littlepal?job=build_macos_x86_64)

## Attribution
Froggy Icon is designed by [OpenMoji](https://openmoji.org/) – the open-source
emoji and icon project. License: [CC BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/#)
